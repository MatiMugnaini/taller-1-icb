# -*- coding: utf-8 -*-
"""
Created on Mon Sep  7 12:36:27 2017

@author: bioinfo
"""
# Ejerercitación 2
import math

def es2N1(res):
    
    r = 0
    for m in range(0,res+1):
        r = ((2**m)-1)
        if r == res:   
            return True

def esPrimo(res):
    sumatoria = 0
    for i in range(1,res+1):
        
        if ((math.fabs(res)%i)==0):
            sumatoria = sumatoria+1
    
    if sumatoria==2:
        return True
            
def Suarez(n):
    
    bigsum=0

    for res in range (1,100+1):
        if es2N1(res) == True and esPrimo(res) == True:
            bigsum=bigsum+1
        
        if bigsum==n:
            return bigsum
            
print(Suarez(4)) 