# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 11:13:43 2017

@author: Matias Mugnaini
"""

# Ejercitación 1

def Dybala(n):##acepta enteros para el calculo del producto y la potencia(aunque los negativos dan cero).
    sumatoria = 0
    for i in range(1,n+1):
        m = ((((-1)**(i+1))*2)/((2*i)-1))
        sumatoria = sumatoria + m
    return sumatoria ##se aproxima a 1.57079... conforme incrementa n.
    
print(Dybala(-1))