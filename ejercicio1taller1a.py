# -*- coding: utf-8 -*-
"""
Created on Mon Sep  4 11:21:55 2017

@author: bioinfo
"""

# Ejercitación 1

def Dybala(n):##acepta enteros para el calculo del producto y la potencia(aunque los negativos dan cero).
    sumatoria = 0
    for i in range(1,n+1):
        m = ((((-1)**(i+1))*2)/((2*i)-1))
        sumatoria = sumatoria + m
    return sumatoria ##se aproxima a 1.57079... conforme incrementa n.
    
print(Dybala(-1))