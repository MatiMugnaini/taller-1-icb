# -*- coding: utf-8 -*-
"""
Created on Sun Sep 10 13:42:50 2017

@author: bioinfo
"""

import math ##para el uso de math.fabs
          
##print(SumatoriaDeRes(127))
res=1 ##acepta enteros para evaluar si satisfacen el criterio de los primos de Mersenne.

def Suarez(n): ##acepta enteros positivos para evaluar cuantos primos de Mersenne hay previos a res(incluyéndolo).
    def es2N1(res):   
        for i in range(0,10): ##rango modificable según la extensión deseada para el calculo de potencias de Mersenne.
            r = ((2**i)-1)
            if r==res:
                return True

    def esPrimo(res):
        sumatoria = 0
        for ii in range(1,res+1):
            
            if ((math.fabs(res)%ii)==0):##la sumatoria incrementa cuando el resto entre res e ii es cero.
                sumatoria = sumatoria+1    
        if sumatoria==2: ##primos, para el caso, son números que satisfacen dos veces la condición del resto igual a cero.
            return True

    def SumatoriaDeRes(res):
        bigsume=0
        for i in range(1,res+1):
            
            if es2N1(i) == True and esPrimo(i) == True: #la gran sumatoria incrementa si en el rango i a res incluye un número Primo de Mersenne.
                bigsume=bigsume+1
        if bigsume==n:##pero si la gran sumatoria no coincide con el input a Suarez, SumatoriaDeRes es falso.
            return True
        else: 
            return False
    if es2N1(res) == True and esPrimo(res) == True and SumatoriaDeRes(res) == True:
        return True##la verdad de la evaluación depende no sólo del estatuto de res sino de la igualdad con n en SumatoriaDeRes.
    else:
        return False
        
print(Suarez(2))
        