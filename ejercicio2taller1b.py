# -*- coding: utf-8 -*-
"""
Created on Fri Sep  8 09:21:39 2017

@author: bioinfo
"""
import math

n=1

def es2N1(res):   
    for n in range(0,10):
        r = ((2**n)-1)
        if r==res:
            return True

def esPrimo(res):
    sumatoria = 0
    for i in range(1,res+1):
        
        if ((math.fabs(res)%i)==0):
            sumatoria = sumatoria+1    
    if sumatoria==2:
        return True

def SumatoriaDeRes(res):
    
    for i in range(0,res+1):
        bigsume=0
        if es2N1(res) == True and esPrimo(res) == True:
            bigsume+=1
    if bigsume==n:
        return True
        
print(SumatoriaDeRes(31))
            
