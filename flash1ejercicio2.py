# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 12:01:22 2017

@author: Matias Mugnaini
"""

def esPrimo(x):
    sumatoria=0
    i=1
    while i<x+1:
        if x%i==0:
            sumatoria+=1
            i+=1
        else:
            i+=1
    if sumatoria==2: #solo si la sumatoria de los divisores entre 1 y x de x es
                    #2, x califica como primo.
        return True
    else:
        return False
        
def es2N1(x):
    i=1
    while i < x+1:
        if ((2**i)-1)==x:
            return x #devuelve x sólo si éste es un número de Mersenne.
        else:
            i+=1
    return False

def Suarez(n):#devuelve el enésimo primo de Mersenne.
    
    i=0 #inicializa el conteo de los Primos de Mersenne.
    j=1 #inicializa el conteo de las potencias con las que va a determinar si  
        #el número resultante es2N1 y esPrimo.
    while i < n:
        j+=1
        if es2N1((2**j)-1) and esPrimo((2**j)-1):
            i+=1        
    return es2N1((2**j)-1)#devuelve, si lo hubiera, el primo de Mersenne
                            #menor o igual al número resultante.
    
print(Suarez(2)) 
 